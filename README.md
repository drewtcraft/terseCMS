## Terse CMS

This is the repository for the front end of the Terse content management system. Using yarn to manage dependencies instead of npm.

I am writing articles about this project as I work on it:
* [Project Goals](https://medium.com/@andrewcraft_11654/terse-revamp-a1761540c1bf)
* [Outline of Features and Sketch](https://medium.com/@andrewcraft_11654/terse-overhaul-cms-outline-428312b35ed6)
* [List of Technologies](https://medium.com/@andrewcraft_11654/terse-overhaul-cms-technologies-ba03259f39b0)
* [Data Structure](https://medium.com/@andrewcraft_11654/terse-overhaul-cms-data-structure-32889bbaa208)



### Technologies
* React
* React Router 
* React Widgets
* React Sortable
* Axios
