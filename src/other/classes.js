const types = ["xPos", "yPos", "alpha", "xScale", "yScale", "rotation"]

function withinRange(start1, end1, start2, end2) {
  if (start1 === undefined || start2 === undefined || start2 === undefined || end2 === undefined) return false
  if (end1 >= start2 && end2 >= start1) return true
  return false
}


class Style {

  constructor (xPos, yPos, alpha, xScale, yScale, rotation) {
    this.xPos = xPos
    this.yPos = yPos
    this.alpha = alpha
    this.xScale = xScale
    this.yScale = yScale
    this.rotation = rotation
  }

  setDefaults () {
    this.xPos = 0
    this.yPos = 0
    this.alpha = 1
    this.xScale = 1
    this.yScale = 1
    this.rotation = 0
  }

  makeStyleFromData (data) {
    this.xPos = data.xPos
    this.yPos = data.yPos
    this.alpha = data.alpha
    this.xScale = data.xScale
    this.yScale = data.yScale
    this.rotation = data.rotation

  }

}

class Event {

  constructor (start, end, startStyle, endStyle) {
    this.start = start
    this.end = end
    this.startStyle = startStyle
    this.endStyle = endStyle
    this.flagged = {}
    this.nonDefaultTypes = {}
  }

  setDefaults () {
    this.start = 0
    this.end = 0
    let startStyle = new Style()
    startStyle.setDefaults()
    this.startStyle = startStyle
    let endStyle = new Style()
    endStyle.setDefaults()
    this.endStyle = endStyle
  }

  get differential() {
      let diff = {}
      if (this.startStyle && this.endStyle) {
        types.forEach((type)=>{
          if (this.startStyle[type] !== this.endStyle[type]) diff[type] = this.endStyle[type] - this.startStyle[type]
        })
      }
      return diff
    }

  get staticProperties () {
    let staticProperties = {}
    if (this.startStyle && this.endStyle) {
      types.forEach((type) => {
        if (Object.keys(this.differential).indexOf(type) === -1) {
          staticProperties[type] = this.startStyle[type]
        }
      })
    }
    return staticProperties
  }

  makeEventFromData (data) {
    this.start = data.startPoint
    this.end = data.endPoint
    let startStyle = new Style()
    startStyle.makeStyleFromData(data.startStyle)
    this.startStyle = startStyle
    let endStyle = new Style()
    endStyle.makeStyleFromData(data.endStyle)
    this.endStyle = endStyle
  }

  setNonDefaultTypes (defaultStyle) {
    this.nonDefaultTypes = {}
    types.forEach((type) => {
      if (this.startStyle[type] !== defaultStyle[type] || this.endStyle[type] !== defaultStyle[type]) {
        this.nonDefaultTypes[type] = null
      }
    })
  }

  getCurrentStyles (scrollPoint, defaultStyle) {

    const nonDefaultTypes = types.filter((type) => {
      return this.startStyle[type] !== defaultStyle[type] || this.endStyle[type] !== defaultStyle[type]
    })

    function interpolate (type) {
      return (scrollPoint / (this.end - this.start)) * this.differential[type] + this.startStyle[type]
    }

    let newStyle = {}
    nonDefaultTypes.forEach((type)=>{
      if (this.differential.hasOwnProperty(type)) newStyle[type] = interpolate.call(this,type)
      else if (this.staticProperties.hasOwnProperty(type)) newStyle[type] = this.staticProperties[type]
    })


    this.currentStyle = newStyle
    return newStyle
  }

}

class Asset {

  constructor (events, assetName, image, defaultStyle, zIndex, width, height) {
    this.assetName = assetName
    this.zIndex = zIndex
    this.image = image
    this.defaultStyle = defaultStyle
    this.hidden = false
    this.events = events !== undefined ? events : []
    this.width = width !== undefined ? width : 100
    this.height = height !== undefined ? height : 100
    this.flagged = false
    this.displayStyle = defaultStyle
  }


  setInitialDefaults () {
    let newStyle = new Style()
    newStyle.setDefaults()
    this.assetName = 'untitled asset'
    this.image = 'noImageSelected'
    this.zIndex = 0
    this.defaultStyle = newStyle
    this.displayStyle = newStyle
    this.hidden = false
    this.events = []
  }

  makeAssetFromData (data) {
    this.assetName = data.assetName
    this.zIndex = data.zIndex
    this.image = data.image
    let defaultStyle = new Style()
    defaultStyle.makeStyleFromData(data.defaultStyle)
    this.defaultStyle = defaultStyle
    this.hidden = data.hidden
    this.width = data.width !== undefined ? data.width : 100
    this.height = data.height !== undefined ? data.height : 100
    this.displayStyle = this.defaultStyle
    let events = data.events.map((event) =>{
      let e = new Event()
      e.makeEventFromData(event)
      return e
    })
    this.events = events
  }

  sortEvents () {
    this.events.sort((a, b)=>{return a.start - b.start})
  }

  setNonDefaultTypesForEvents () {
    this.events.forEach((event)=>{
      event.setNonDefaultTypes(this.defaultStyle)
    })
  }

  validateNewEvent (idx) {

    let conflictingTypes = []
    this.setNonDefaultTypesForEvents()
    const newEvent = this.events[idx]
    this.events.forEach((event, i) => {
      if (i !== idx && withinRange(newEvent.start, newEvent.end, event.start, event.end)) {
        Object.keys(newEvent.nonDefaultTypes).forEach((type) => {
          if (event.nonDefaultTypes.hasOwnProperty(type)) {
            conflictingTypes.push(type)
          }
        })
      }
    })
    return conflictingTypes
  }



  setDefaultValue (e, type) {
    this.defaultStyle[type] = e.target.value
  }

  isDefault (event, type) {
    return event.startStyle[type] === this.defaultStyle[type] && event.endStyle[type] === this.defaultStyle[type]
  }

  duplicatesPresent (idx, type) {
    const newEvent = this.events[idx]
    const coincidingEvents = this.events.filter((event, i) => {
        return i !== idx && withinRange(newEvent.start, newEvent.end, event.start, event.end)
      })
    let flagged = false
    coincidingEvents.forEach((event) => {
      if (newEvent.startStyle[type] !== this.defaultStyle[type] || newEvent.endStyle[type] !== this.defaultStyle[type]) flagged = true
      if (flagged) event.flagged[type] = null
    })
    if (flagged) {
      newEvent.flagged[type]
      return type
    }
    return false
  }


    validateEvents () {
      let conflictTypes = []
      types.forEach((type) => {
        for (let i = 0;i < this.events.length - 1; i++) {
            for (let g = i + 1; g < this.events.length; g++) {
              if (!this.isDefault(this.events[i], type) && !this.isDefault(this.events[g], type)) {
                if (withinRange(this.events[i].start, this.events[i].end, this.events[g].start, this.events[g].end)) {
                  this.events[i].flagged[type] = null
                  this.events[g].flagged[type] = null
                  this.flagged = true
                  conflictTypes.push(type)
                }
              }
              else {
                delete this.events[i].flagged[type]
                delete this.events[g].flagged[type]
              }
            }
        }
      })
      if (conflictTypes.length > 0) {
        this.flagged = true
      } else {
        this.flagged = false
      }
              return conflictTypes

    }


  deleteEvent (event) {
    let index = this.events.indexOf(event)
    if (index > -1) {
      this.events.splice(index, 1)
    }
  }

  addNewEvent () {
    const newEvent = new Event()
    newEvent.setDefaults()
    this.events.push(newEvent)
    this.sortEvents()
  }

  //I want to rewrite this code!
  getRelevantStyles (scrollPoint) {
    if (this.events.length > 0) {
      return this.events.map((event)=>{
        if (event.start <= scrollPoint && event.end >= scrollPoint && event.start !== event.end) {
           event.getCurrentStyles(scrollPoint, this.defaultStyle)
           event.setNonDefaultTypes(this.defaultStyle)
           return event
        }
      }).filter((event) => {return event !== undefined})
    }
    return []
  }

  getCurrentStyle (scrollPoint) {

    let relevantStyles = this.getRelevantStyles(scrollPoint)


    if (relevantStyles.length === 0) {
      this.displayStyle = this.defaultStyle
      return this.defaultStyle
    }


    let newStyle = new Style()
    Object.keys(this.defaultStyle).forEach((key) => {
      newStyle[key] = this.defaultStyle[key]
    })

    if (relevantStyles.length > 1) {
      relevantStyles.forEach((event)=>{
        Object.keys(event.nonDefaultTypes).forEach((type)=>{
          newStyle[type] = event.currentStyle[type]
        })
      })
    }
    else if (relevantStyles.length === 1) {
      Object.keys(relevantStyles[0].nonDefaultTypes).forEach((type)=>{
          newStyle[type] = relevantStyles[0].currentStyle[type]
        })
    }
    this.displayStyle = newStyle
    return newStyle
  }

  // MUST EVENTUALLY FACOTR IN STRANGE DATA TYPES! e.g. top of screen

  makeStyleObject (scrollPoint, screenSize) {
    let currentStyle = this.getCurrentStyle(scrollPoint)

    let style = {}
    style.opacity = `${currentStyle.alpha}`
    style.top = `${(screenSize.height/2) - (this.height/2) + currentStyle.yPos}px`
    style.left = `${(screenSize.width/2) - (this.width/2) + currentStyle.xPos}px`
    style.transform = `scale(${currentStyle.xScale}, ${currentStyle.yScale}) rotate(${currentStyle.rotation}deg)`
    style.WebkitTransform = `scale(${currentStyle.xScale}, ${currentStyle.yScale}) rotate(${currentStyle.rotation}deg)`
    style.backgroundImage = `${this.image}`
    style.zIndex = 600 - this.zIndex

    this.currentStyle = style

  }
}

class Series {

  constructor (id, title, stories) {
    this.title = title
    this.stories = stories
  }

  insertStory (story, index) {
    this.stories.splice(index, 0, story)
  }

  removeStory (story) {
    let index = this.stories.indexOf(story);
    if (index > -1) {
      this.stories.splice(index, 1);
    }
  }

}

class Story {

  constructor (assets, title, summary, author, illustrator, series) {
    this.assets = assets ? assets.map((asset, idx)=>{
      asset.zIndex = idx
      return asset
    }) : []
    this.title = title
    this.summary = summary
    this.author = author
    this.illustrator = illustrator
    this.series = series
    this.users = []
    this.flagged = false
    this.max = 100
    this.dateModified = new Date()
  }

  // setCurrentStylesForAllAssets (scrollPoint) {
  //   return this.assets.map((asset) =>{
  //     asset.getCurrentState (scrollPoint)
  //     return asset
  //   })
  // }

  validateAllAssets () {
    let conflicts = {}
    this.assets.forEach((asset) => {
      conflicts[asset.assetName] = asset.validateEvents()
    })
    return conflicts
  }

  setCurrentStylesForAllAssets (scrollPoint, screenSize) {
    return this.assets.map((asset) => {
      asset.makeStyleObject (scrollPoint, screenSize)
      return asset
    })
  }

  addAsset (asset) {
    if (asset) asset.zIndex = this.assets.length
    this.assets.push(asset)

  }

  toggleFlag (flag, assetIdx, eventIdx, type) {
    if (flag) {
        this.flagged = true
        this.assets[assetIdx].flagged = true
        this.assets[assetIdx].events[eventIdx].flagged[type]
      }
    else {

      if (this.validateAllAssets()){
        let flagAsset = false
        this.assets[assetIdx].events.forEach((event)=>{
          if (Object.keys(event.flagged).length > 0) flagAsset = true
        })
        this.assets[assetIdx].flagged = flagAsset

        let flagStory = false
        this.assets.forEach((asset)=>{
          if (asset.flagged) flagStory = true
        })
        this.flagged = flagStory
      }
    }
  }

  storyFromData (data, screenSize) {

    let assets = data.assets.map((asset) => {
      let a = new Asset()
      a.makeAssetFromData(asset)
      return a
    })

    this.title = data.title
    this.author = data.author
    this.illustrator = data.illustrator
    this.summary = data.summary
    this.series = data.series
    this.assets = assets
    this.max = data.max
    this.dateModified = data.dateModified
    this._id = data._id


    this.setCurrentStylesForAllAssets(0, screenSize)
  }

}

module.exports = {Asset, Event, Style, Series, Story}

