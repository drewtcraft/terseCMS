import {Asset, Story, Event, Style, Series} from './classes.js';

let initialStyle = new Style()
initialStyle.setDefaults()
let style1 = new Style(0, 10, 0.5, 1, 1, 0)
let style2 = new Style(0, 500, 0.8, 1, 1, 0)
let event1 = new Event(300, 800, style1, style2)
let style3 = new Style(0, 10, 0.5, 1, 1, 0)
let style4 = new Style(0, 500, 0.8, 1, 1, 0)
let event2 = new Event(0, 800, style3, style4)
const todayString = `${new Date()}`

let testStory = new Story([], 'untitled story', 'replace with summary', 'author', 'illustrator', undefined)

testStory.dateModified = todayString

export {testStory}

