import React from 'react';
import {Link} from 'react-router-dom';


import '../../styles/header.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTh, faPlusSquare, faSchool, faUniversity } from '@fortawesome/free-solid-svg-icons'
import { faNewspaper } from '@fortawesome/free-regular-svg-icons'


library.add(faTh)
library.add(faPlusSquare)




export default function Header (props) {

  const linkClass = "link"
  const navLinkClass = "link navLink"

  let pages = (
    <nav>
      <Link className={props.location === '/tutorials' ? `${linkClass} active` : `${linkClass}`} to={{pathname: `/tutorials`}}>tutorials</Link>
      <Link className={props.location === '/register' ? `${navLinkClass} active` : `${linkClass}`} to={{pathname: '/register'}}>register</Link>
      <Link className="link navLink" to={{pathname: '/login'}}>log in</Link>
    </nav>
    )

  if (props.user) {

    pages = (
      <nav>
        <Link className={props.location === '/tutorials' ? `${linkClass} pressed` : `${linkClass}`} to={{pathname: `/tutorials`}}>tutorials</Link>
        <Link className={props.location === '/newStory' ? `${linkClass} pressed` : `${linkClass}`} to={{pathname: `/newStory`}}>new story</Link>
        <Link className={props.location === '/stories' ? `${linkClass} pressed` : `${linkClass}`} to={{pathname: `/stories`}}>my stories</Link>
                <div className="link" onClick={props.logOut}>log out</div>

      </nav>
      )
  }

  return (
    <header>
      <div className="home">
        <Link className="link headerTitle" to={{pathname: '/'}}>Terse CMS</Link><div className="beta">beta</div>
      </div>
      {pages}
    </header>
    )
}
