import React from 'react';
import '../../styles/tutorials.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { faEdit, faSave } from '@fortawesome/free-regular-svg-icons'

export default class TutorialFeed extends React.Component {
  constructor(props){
    super(props)
  }

  render () {
    return (
      <div className="tutorialFeed">
        <h4>Tutorials</h4>
        <p>No real tutorials yet, but here's a shitty one to get you started:</p>
        <ol>
          <li>click "new story" in the navigation bar</li>
          <li>click the <FontAwesomeIcon icon={faPlusCircle} /> button in the lefthand column to add a new asset</li>
          <li>select the image for the asset</li>
          <li>after it is added, click the asset's cell to select it</li>
          <li>click the <FontAwesomeIcon icon={faPlusCircle} /> in the righthand column to add an event for that asset</li>
          <li>click the ▼ arrow to edit the properties of the event</li>
          <li>click the ▶ button or drag the handle on the rangeslider to preview the animation</li>
          <li>click the <FontAwesomeIcon icon={faSave} /> icon to save the story</li>
          <li>click the <FontAwesomeIcon icon={faEdit} /> icon to edit the title, contributors, and summary for the story</li>
        </ol>
      </div>
      )
  }

}
