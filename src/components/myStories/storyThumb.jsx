import React from 'react';
import { withRouter } from 'react-router'
import axios from 'axios';
import './../../styles/myStories.css'

class StoryThumb extends React.Component {
    constructor(props) {
      super(props)
    }

    handleClick = async () => {
      await this.props.setID(this.props._id)
      this.props.history.push('/story')
    }

    formatDate = () => {

    }

    render() {

      const date = new Date(this.props.dateModified)
      const day = date.getDate()
      const year = date.getFullYear()
      const month = date.getMonth() + 1
      const hours = date.getHours() % 12
      const minutes = date.getMinutes()
      const formattedDate = `${month}/${day}/${year} ${hours === 0 ? 12 : hours }:${minutes}${date.getHours() < 12 ? 'am' : 'pm'}`

      return (
        <div className="storyThumb" onClick={this.handleClick}>
          <h2>{this.props.title.length < 55 ? this.props.title : `${this.props.title.split('').splice(0, 52).join("")}...`}</h2>
          <p>Last edited: {formattedDate}</p>
          <div>{this.props.published ? 'published' : 'unpublished'}</div>
          <button onClick={()=>{this.props.deleteStory(this.props._id)}}>delete</button>
        </div>
        )
  }

}

export default withRouter(StoryThumb)
