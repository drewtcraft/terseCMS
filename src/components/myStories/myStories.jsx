import React from 'react';
import StoryThumb from './storyThumb.jsx'
import Loading from '../loading.jsx'
import axios from 'axios';
import './../../styles/myStories.css'

export default class MyStories extends React.Component {

  constructor (props) {
    super (props)

    this.state = {
      stories: [{title: 'test title bitch', date: '08/15/2018'}, {title: 'much longer test title bitch: test title 2.000000 yaherd? The sequel', date: '12/31/2018'}],
      loading: true
    }

    this.getStories = this.getStories.bind(this)
  }

  componentDidMount () {
    this.getStories()
  }

  getStories () {
    const token = window.localStorage.getItem('terseCMSToken')


    axios.post(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/stories`, {token: token})
      .then((data) => {
        this.setState({
          stories: data.data.stories,
          loading: false
        })
      })
      .catch((err) => {console.log(err)})
  }


    deleteStory = (id) => {
      const token = window.localStorage.getItem('terseCMSToken')

      axios.post(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/deleteStory`, {token: token, _id: id})
        .then((res) => {
          this.setState((prevState)=>
           { return { stories: prevState.stories.filter((story) => {return story._id !== id})}}
            )
        })
        .catch((err) => {
          console.log(err)
        })
    }




  //function for selecting a story, maybe passed down from app.jsx

  render () {

    if (this.state.loading) return <Loading />

    let stories = []
    for (let i = this.state.stories.length-1; i >= 0; i--) {
      stories.push(<StoryThumb {...this.state.stories[i]} key={i} setID={this.props.setID} deleteStory={this.deleteStory} />)
    }

    let emptyStories = []
    for (let i = 0; i <= 20 - stories.length; i++) {
      emptyStories.push(<div className="empty" key={i}></div>)
    }

    return (
      <div className="myStories">
        <h4>My Stories</h4>
        <div className="stories">
          {stories}
          {emptyStories}
        </div>
      </div>
      )
  }
}
