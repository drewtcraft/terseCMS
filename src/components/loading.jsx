import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import '../styles/App.css'

export default function Loading (props) {
  return (
    <div className="loading">
    <FontAwesomeIcon icon={faCircleNotch} />
    </div>
    )
}
