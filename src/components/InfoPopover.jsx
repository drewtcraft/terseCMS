import React from 'react';

export default function InfoPopover (props) {
  return (
    <div className={props.showInfo ? "popover" : "hidden"} onClick={props.hidePopover}>
      <h4>{props.title}</h4>
      <p>{props.text}</p>
    </div>
    )
}
