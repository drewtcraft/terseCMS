import React from 'react';
import { withRouter } from 'react-router-dom';

export default function requireAuth(Component, props) {

  class AuthenticatedComponent extends React.Component {

    componentWillMount() {
      this.checkAuth();
    }

    checkAuth = () => {
      if ( !props.user) {
        console.log('fuck')
        this.props.history.push('/login')
      }
    }

    render() {
      return (props.user ? <Component { ...props } /> : null);
    }

  }

  let X = withRouter(AuthenticatedComponent);

  return <X />
}
