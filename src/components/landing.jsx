import React from 'react';
import '../styles/landing.css';


export default class Landing extends React.Component {
  constructor(props) {
    super(props)
  }
  render () {
    return (
      <div className="landing">
      <div>
      <h2>Terse CMS</h2>
      <br />
        This is the Terse Content Management System beta to support the <a href="https://itunes.apple.com/us/app/terse-very-short-stories/id1239243048?mt=8">Terse Mobile App</a>.
        <br /><br />
        If you are a beta tester, please log in and start making stuff.
        <br /><br />
        If you are not a beta tester and would like to be one, just register for an account and get going.
        <br /><br />
        Email me with bugs/issues/suggestions at: drewtcraft@gmail.com
        <br /><br />
        </div>
      </div>
      )
  }
}
