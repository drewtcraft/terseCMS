import React from 'react';
import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';
import AssetCell from './assetCell.jsx';
import StoryInfo from './storyInfo.jsx';
import '../../styles/cms.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit } from '@fortawesome/free-regular-svg-icons'
import {  faPlusCircle } from '@fortawesome/free-solid-svg-icons'


const SortableItem = SortableElement (({asset, setSelected, idx, selected, deleteAsset, hideAsset, editAsset}) =>
    <AssetCell {...asset} setSelected={setSelected} idx={idx} selected={selected} deleteAsset={deleteAsset} hideAsset={hideAsset} editAsset={editAsset}  />
  );

const SortableList = SortableContainer(({items, setSelected, selected, deleteAsset, hideAsset, editAsset}) => {
  return (
    <ul>
      {items.map((value, i) => (
        <SortableItem key={i} index={i} asset={value} setSelected={setSelected} idx={i} selected={selected} deleteAsset={deleteAsset} hideAsset={hideAsset} editAsset={editAsset} />
      ))}
    </ul>
  );
});


export default class AssetContainer extends React.Component {

  constructor(props){
    super(props)

  }

  componentDidMount(){
  }

  render(){

    const date = new Date(this.props.dateModified)
      const day = date.getDate()
      const year = date.getFullYear()
      const month = date.getMonth() + 1
      const hours = date.getHours() % 12
      const minutes = date.getMinutes()
      const formattedDate = `last saved: ${month}/${day}/${year} ${hours === 0 ? 12 : hours }:${minutes}${date.getHours() < 12 ? 'am' : 'pm'}`

    return(
      <div className="assetContainer">
        <StoryInfo
          saveStory={this.props.saveStory}
          toggleStoryInfoPopover={this.props.toggleStoryInfoPopover}
          title={this.props.title} />
        <div className="dateModified">{formattedDate}</div>
        <h4>Assets</h4>
        <button className="add" onClick={this.props.toggleImageUploader}><FontAwesomeIcon icon={faPlusCircle} /></button>
        <SortableList
          selected={this.props.selected}
          items={this.props.assets}
          setSelected={this.props.setSelected}
          onSortEnd={this.props.onSortEnd}
          useDragHandle={true}
          lockAxis={'y'}
          deleteAsset={this.props.deleteAsset}
          hideAsset={this.props.hideAsset}
          editAsset={this.props.editAsset} />
      </div>
      )
  }
}
