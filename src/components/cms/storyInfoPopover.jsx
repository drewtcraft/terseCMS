import React from 'react';

export default class StoryInfoPopover extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      trueStoryteller: false
    }
  }

  render () {
    let form

      form = (
        <div className={this.props.active ? 'active storyInfoPopover' : 'storyInfoPopover'}>
          <h4>edit story information</h4>
          <p>title</p>
          <input type="text" onChange={(e)=>{this.props.handleStoryInfoEdit(e, 'title')}} value={this.props.title} />
          <p>author</p>
          <input type="text" onChange={(e)=>{this.props.handleStoryInfoEdit(e, 'author')}} value={this.props.author} />
          <p>illustrator</p>
          <input type="text" onChange={(e)=>{this.props.handleStoryInfoEdit(e, 'illustrator')}} value={this.props.illustrator} />
          <p>summary</p>
          <textarea type="text" onChange={(e)=>{this.props.handleStoryInfoEdit(e, 'summary')}} value={this.props.summary} />
          <button onClick={this.props.toggleStoryInfoPopover}>save</button>
        </div>
      )


    return (
      <div>
      {form}
      </div>
      )
  }
}
