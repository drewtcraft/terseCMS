import React, { Component } from 'react';
import axios from 'axios';
import '../../styles/cms.css';


export default class ImageUploader extends Component {
  constructor (props) {
    super(props);
    this.state = {
      files: null
    };
  }

  submitFile = (event) => {
        const token = window.localStorage.getItem('terseCMSToken')
        console.log(token)
    event.preventDefault();
    if (this.state.files) {
      Array.from(this.state.files).forEach((file, i)=>{
        const formData = new FormData();
        formData.append('file', file);
        axios.post(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/images`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(res => {
          console.log(res.data)
          this.props.addAsset(res.data)
          if (i === this.state.files.length - 1) this.props.toggleImageUploader()
        }).catch(err => {
          console.log(err)
        });
      })
    }
  }

  handleFileUpload = (event) => {
    event.preventDefault()
    this.setState({files: event.target.files});
  }

  render () {
    let imageUploaderClassName = 'imageUploader'
    if (!this.props.showImageUploader) imageUploaderClassName += ' hidden'
    return (
      <form className={imageUploaderClassName} onSubmit={this.submitFile}>
        <h3>upload image(s)</h3>
        <div className="file-input-wrapper">
        <button className="btn-file-input" onClick={() => {this.fileInput.click()}}>choose image</button>
        <input type="file" accept="image/png" name="file" ref={fileInput => {this.fileInput = fileInput}} onChange={this.handleFileUpload} multiple  />
        </div>
        <div>
        <button type='submit'>upload</button>
        <button onClick={()=>{
          this.props.toggleImageUploader()
          this.setState({files: null})
        }}>cancel</button>
        </div>
      </form>
    );
  }
}

