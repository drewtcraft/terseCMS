import React from 'react';
import {SortableHandle} from 'react-sortable-hoc';
import {Asset} from '../../other/classes.js';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

library.add(faEye)
library.add(faEyeSlash)


const DragHandle = SortableHandle(() => <span className="dragHandle"></span>)

export default class AssetCell extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      expanded: false,
      selected: false,
      hidden:false
          }
    this.expand = this.expand.bind(this)
    this.select = this.select.bind(this)
    this.hide = this.hide.bind(this)
    this.handleContentChange = this.handleContentChange.bind(this)
  }

  componentDidMount () {

  }

  select (e) {
    e.preventDefault()
    this.props.setSelected(this.props.idx)
  }

  expand (e) {

    e.stopPropagation()
    this.setState((prevState)=>{
      return {
        expanded: !prevState.expanded
      }
    })

  }

  hide (e) {
    e.stopPropagation()
    this.props.hideAsset(this.props.idx)
    this.setState((prevState) => {
      return {hidden: !prevState.hidden}
    })

  }

  handleContentChange (e, type, idx) {
    console.log(e.target.value)
    this.props.editAsset(e.target.value, type, idx)

    //set changes in parent attribute as well with this.state.content

  }

  render () {

    let cellContent = (<div></div>)
    if (this.state.expanded) {
      cellContent = (
        <div className='cellForms'>
          <p>asset name</p>
          <input type="text" value={this.props.assetName} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'assetName', this.props.idx)}} />
          <p className="defaultStyle">default style</p>
          <p>position:</p>
          <div className="coordinate">
            <p>x</p>
            <input type="number" min="0" value={this.props.xPos} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'xPos', this.props.idx)}} />
            <p>y</p>
            <input type="number" min="0" value={this.props.yPos} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'yPos', this.props.idx)}} />
          </div>
          <p>opacity</p>
          <input type="number" min="0" max="1" value={this.props.alpha} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'alpha', this.props.idx)}} />
          <p>scale</p>
          <div className="coordinate">
            <p>x</p>
            <input type="number" min="0" value={this.props.xScale} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'xScale', this.props.idx)}} />
            <p>y</p>
            <input type="number" min="0"  value={this.props.yScale} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'yScale', this.props.idx)}} />
          </div>

          <p>rotation</p>
          <input type="number" min="0" max="1" value={this.props.rotation} onClick={(e)=>{e.stopPropagation()}} onChange={(e)=>{this.handleContentChange(e, 'rotation', this.props.idx)}} />

          <button className="deleteAsset" onClick={(e)=>{
            e.stopPropagation()
            this.props.deleteAsset(this.props.idx)}}><FontAwesomeIcon icon={faTrashAlt} /></button>
        </div>
      )
    } else if (this.props.assetName) {
      cellContent = (
        <div className='cellContent'>
          <div className="imageContainer">
            <img src={this.props.image} alt="assetImage" />
          </div>
          <div>
            <h2>{this.props.assetName}</h2>
            <p>x: {Math.round(this.props.displayStyle.xPos* 100) / 100}, y: {Math.round(this.props.displayStyle.yPos* 100) / 100}</p>
            <p>alpha: {Math.round(this.props.displayStyle.alpha * 100) / 100}</p>
            <p>scale: ({Math.round(this.props.displayStyle.xScale * 100) / 100}, {Math.round(this.props.displayStyle.yScale * 100) / 100})</p>
            <p>rotation: {Math.round(this.props.displayStyle.rotation * 100) / 100} deg</p>
          </div>
        </div>
      )
    }

    let assetClassName = this.state.expanded ? "asset expanded" : "asset"


    this.props.selected === this.props.idx ? assetClassName += " selected" : null
    this.props.flagged ? assetClassName += ' flagged' : null

    return (
        <li className={assetClassName} onClick={this.select}>
        <div className={this.state.hidden ? "eyeball red" : "eyeball"} onClick={this.hide}><FontAwesomeIcon icon={this.state.hidden ? "eye-slash" : "eye"} /></div>
          <DragHandle />
          {cellContent}
          <button className="expandArrow" onClick={this.expand}>{this.state.expanded ? "▲" : "▼"}</button>
        </li>

    )
  }
}
