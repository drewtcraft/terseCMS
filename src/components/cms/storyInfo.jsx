import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faSave } from '@fortawesome/free-regular-svg-icons'
import { faArrowCircleUp } from '@fortawesome/free-solid-svg-icons'


export default function StoryInfo (props) {
  return (
    <div className="storyInfo">
      <h2>{props.title.length > 18 ? props.title.slice(0, 15) + "..." : props.title}</h2>
      <div>
      <button title="edit story information" onClick={props.toggleStoryInfoPopover}><FontAwesomeIcon icon={faEdit} /></button>
      <button title="save" onClick={props.saveStory}><FontAwesomeIcon icon={faSave} /></button>
      <button title="submit to publish"><FontAwesomeIcon icon={faArrowCircleUp} /></button>
      </div>
    </div>
    )
}
