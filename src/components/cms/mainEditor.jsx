import React from 'react';
import PreviewContainer from './preview.jsx';
import AssetContainer from './assetContainer.jsx';
import EventContainer from './eventContainer.jsx';
import StoryInfoPopover from './storyInfoPopover.jsx';
import ImageUploader from './imageUploader.jsx';
import {arrayMove} from 'react-sortable-hoc';
import {Asset, Story, Event, Style, Series} from '../../other/classes.js';
import {testStory} from '../../other/testStory.js';
import Loading from '../loading.jsx'

import axios from 'axios';

let timer

export default class MainEditor extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      selected: {events:[], idx: null},
      story: testStory,
      showStoryInfoPopover: false,
      scrollPoint: 0,
      playing: false,
      screenSize: { width:333, height: 592},
      previewAssets: testStory.assets,
      showImageUploader: false,
      fader:false,
      loading: true
      }

    this.setSelected = this.setSelected.bind(this)
    this.onSortEnd = this.onSortEnd.bind(this)
    this.editEventProperty = this.editEventProperty.bind(this)
    this.addEvent = this.addEvent.bind(this)
    this.deleteEvent = this.deleteEvent.bind(this)
    this.addAsset = this.addAsset.bind(this)
    this.hideAsset = this.hideAsset.bind(this)
    this.deleteAsset = this.deleteAsset.bind(this)
    this.toggleStoryInfoPopover = this.toggleStoryInfoPopover.bind(this)
    this.handleStoryInfoEdit = this.handleStoryInfoEdit.bind(this)
    this.setScrollPoint = this.setScrollPoint.bind(this)
    this.playPreview = this.playPreview.bind(this)
    this.stopPreview = this.stopPreview.bind(this)
    this.updateMain = this.updateMain.bind(this)
    this.toggleImageUploader = this.toggleImageUploader.bind(this)
    this.saveStory = this.saveStory.bind(this)
  }

  componentDidMount () {
    if (this.props.id)  this.getStory()
    else if (this.props.new) {
      this.setState({loading:false, story: testStory, previewAssets: []})
    }

  }



  getStory = () => {

    const token = window.localStorage.getItem('terseCMSToken')

       axios.post(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/story`, {
         token: token,
         id: this.props.id
       })
         .then((data) => {
           let newStory = new Story()
           newStory.storyFromData(data.data.story, this.state.screenSize)
           this.setState({
             story: newStory,
             loading: false
           })
         })
         .catch((err) => {console.log(err)})

  }

  toggleImageUploader () {
    this.setState((prevState) => {
      return {
        showImageUploader: !prevState.showImageUploader,
        fader: !prevState.fader
      }
    })
  }



  saveStory () {

    const token = window.localStorage.getItem('terseCMSToken')

    let path = 'newStory'

    if (!this.state.story._id) {
      this.state.story.user = this.props.user

      axios.post(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/newStory`, {
        token: token,
        story: this.state.story
      })
      .then((res) => {
        this.setState((prevState) => {
          let newStory = prevState.story
          newStory._id = res.data
          newStory.dateModified = new Date()
          return {story: newStory}
        })
      })
      .catch((err) => {
        console.log(err)
      })
    }
    else {
      axios.put(`http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/story`, {
        token: token,
        story: this.state.story
      })
      .then((res) => {
        console.log(res)
      })
      .catch((err) => {
        console.log(err)
      })
    }

  }

  validateEvent = (idx, type) => {
    this.setScrollPoint(this.state.scrollPoint)
    if (typeof this.state.selected.idx !== 'null') {

      const assetIndex = this.state.selected.idx
      const asset = this.state.story.assets[assetIndex]
      const currentEvent = asset.events[idx]

      if (currentEvent.end > this.state.story.max) this.state.story.max = currentEvent.end

      if (currentEvent.start > currentEvent.end) {
        this.props.presentInfoPopover("FORBIDDEN", `Cannot set end scroll point lower than start scroll point.`)
        currentEvent.start = currentEvent.end - 1
        return
      }

      asset.setNonDefaultTypesForEvents(asset.defaultStyle)

      let conflictingTypes = {}

      asset.events.forEach((event, i) => {
        if (i !== idx) {
          // for each asset that intersects with the asset in question
          if (currentEvent.end >= event.start && event.end >= currentEvent.start) {
            const relevantTypes = Object.keys(currentEvent.nonDefaultTypes)
            for (let t in relevantTypes) {
              if (event.nonDefaultTypes.hasOwnProperty(t)) {
                conflictingTypes[t] = null
              }
            }

          }
        }
      })

      if (Object.keys(conflictingTypes).length > 0) {
        conflictingTypes = Object.keys(conflictingTypes)
        let strErrTypes = conflictingTypes.join(', ')
        if (strErrTypes[strErrTypes.length - 2] === ',') strErrTypes.splice(0, strErrTypes.length - 2)
        this.props.presentInfoPopover("FORBIDDEN", `Cannot set ${strErrTypes} values for simultaneous events. Reverting ${strErrTypes} to asset defaults.`)
        conflictingTypes.forEach((type)=>{
          currentEvent.startStyle[type] = asset.defaultStyle[type]
          currentEvent.endStyle[type] = asset.defaultStyle[type]
        })
        currentEvent.startStyle[type] = asset.defaultStyle[type]
        currentEvent.endStyle[type] = asset.defaultStyle[type]

        return
      }

      this.setState((prevState) => {
        let newStory = prevState.story
        newStory.assets[assetIndex] = asset
        return {story: newStory, selected: prevState.selected}
      })


    }
  }


  updateMain () {
    this.forceUpdate()
  }

  toggleStoryInfoPopover (e) {
    e.preventDefault()
    this.setState((prevState)=>{
      return {
        showStoryInfoPopover: !prevState.showStoryInfoPopover,
        fader: !prevState.fader
      }
    })
  }

  handleStoryInfoEdit (e, type) {

    e.preventDefault()
    let newStory = this.state.story
    newStory[`${type}`] = e.target.value

    this.setState((prevState)=>{

      return {story: newStory}
    })

  }

  addAsset (data) {

    let newAsset = new Asset()
    newAsset.setInitialDefaults()
    newAsset.image = data.url
    newAsset.width = data.dimensions.width
    newAsset.height = data.dimensions.height


    this.setState((prevState) => {
      let newStory = prevState.story
      newStory.assets.push(newAsset)
      newStory.assets = newStory.assets.map((asset, i) => {
        asset.zIndex = i
        return asset
      })
      return {
        story: newStory,
        previewAssets: newStory.assets
      }
    })

    this.setScrollPoint(0, this.state.screenSize)

  }

  deleteAsset (idx) {

    axios.post('http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/cms/deleteImage', {path: this.state.story.assets[idx].image})
      .then((res) => {
        console.log(res.data)
      })
      .catch((err) => {
        console.log(err)
      })

    this.setState((prevState) => {
      let newStory = prevState.story
      newStory.assets = newStory.assets.filter((_, _idx) => idx !== _idx)
      return {
        story: newStory,
        previewAssets: newStory.assets
      }
    })

  }

  setScrollPoint (scrollPoint) {
    if (scrollPoint <= this.state.story.max) {
      this.setState((prevState) => {
        return {
          scrollPoint:scrollPoint,
          previewAssets: prevState.story.setCurrentStylesForAllAssets(this.state.scrollPoint, this.state.screenSize)
        }
      })
    }
  }

  playPreview (scrollPoint) {

    if (this.state.story.flagged) {
      this.props.presentInfoPopover('FIX ERRORS', 'sorry, but you cannot play the story while assets and events are flagged.')
      return
    }

    this.setState({playing:true})

    if (scrollPoint === this.state.story.max) scrollPoint = 0

    this.setScrollPoint(scrollPoint)
    timer = setInterval(()=>{
      if (scrollPoint < this.state.story.max) { this.setScrollPoint(scrollPoint+=1) }
      else {
        this.setState({playing:false})
        clearInterval(this.state.interval)
      }

    }, 5)
    this.setState({
      interval: timer
    })
  }

  stopPreview () {
    this.setState({playing:false})
    clearInterval(this.state.interval)
    this.setScrollPoint(this.state.scrollPoint)
  }

  editAsset = (value, type, idx) => {

    this.setState((prevState) => {
      let newStory = prevState.story
      console.log(newStory)
      let newAsset = newStory.assets[idx]

      console.log(newAsset)
      if (type === 'assetName') { newAsset.assetName = value }
      else {
        newAsset.defaultStyle[type] = parseFloat(value)
      }
      newStory.assets[idx] = newAsset
      return {story: newStory}
    })
  }

  hideAsset (idx) {
    this.setState((prevState) => {
      let newStory = prevState.story
      newStory.assets = newStory.assets.map((asset, _idx) => {
        if (idx === _idx) {
          let newAsset = asset
          newAsset.hidden = !asset.hidden
          return newAsset
        }
        return asset
      })
      return {
        story: newStory
      }
    })
  }

  setSelected (idx) {

    console.log('selected index', idx)
    this.state.story.assets[idx].setNonDefaultTypesForEvents()
    this.setState((prevState) => {
      return prevState.selected.idx === idx ? {selected: {assetName: '', events: [], idx: null}} : {selected: {assetName: prevState.story.assets[idx].assetName, events: prevState.story.assets[idx].events, idx: idx}}
    })

  }

  onSortEnd ({oldIndex, newIndex}) {

      let newSelected = this.state.selected
      if (oldIndex === this.state.selected) {
        newSelected = newIndex
      } else if (newIndex >= this.state.selected && oldIndex <= this.state.selected) {
        newSelected = this.state.selected - 1
      } else if (newIndex <= this.state.selected && oldIndex >= this.state.selected) {
        newSelected = this.state.selected + 1
      }
      this.setState((prevState) => {
        let newStory = prevState.story
        newStory.assets = arrayMove(this.state.story.assets, oldIndex, newIndex)
        newStory.assets = newStory.assets.map((asset, i) => {
          console.log(asset, i)
          asset.zIndex = i
          return asset})
        return {
          story: newStory,
          selected: newSelected,
          previewAssets: newStory.assets
        }
      });

      this.setScrollPoint(0, this.state.screenSize)


    };

  editEventProperty (e, type, idx, style) {

    if (typeof this.state.selected.idx === 'number') {
      console.log('firing')
      let assetIdx = this.state.selected.idx
      const value = parseFloat(e.target.value)
      this.setState((prevState) => {
        let newStory = prevState.story
        if (!style) {
          newStory.assets[assetIdx].events[idx][type] = value
        }
        else {
          if (style === 'start') {
            console.log(newStory.assets[assetIdx].events[idx].startStyle[type])
            newStory.assets[assetIdx].events[idx].startStyle[type] = value
          } else {
            newStory.assets[assetIdx].events[idx].endStyle[type] = value
          }
        }
        return {story: newStory}
      })
    }

  }

  commitEvent (asset, oldEvent, newEvent) {

    let assetIndex = this.state.story.assets.indexOf(asset)
    let eventIndex = this.state.story.assets[assetIndex].indexOf(oldEvent)
    if (assetIndex > -1 && eventIndex > -1) {
      const types = ["xPos", "yPos", "alpha", "xScale", "yScale", "rotation"]
      const coincidingEvents = this.state.asset[assetIndex].events.filter((event, i) => {
        //if the event is a different index and the ranges intersect
        return i !== eventIndex && (newEvent.start >= event.start && newEvent.start <= event.end || newEvent.end <= event.end && newEvent.end >= event.start || event.start >= newEvent.start && event.start >= newEvent.end || event.end <= newEvent.end && event.end >= newEvent.start)
      })

      //bool to break outer for .. in and return
      let brk = false
      for (let e in coincidingEvents) {
        for (let t in types) {
          if (e.differential.hasOwnProperty(t) && newEvent.differential.hasOwnProperty(t)) {
            brk = true
            this.props.presentInfoPopover("FORBIDDEN", `Cannot set ${t} for simultaneous events. Please `)
            break
          }
        }
        if (brk) break
      }

      if (brk) return

      this.setState((prevState) => {
        let newStory = prevState.story
        newStory.assets[assetIndex].editEvent(newEvent)
        return {story: newStory}
      })
    }

  }

  addEvent () {
    let assetIndex = this.state.selected.idx
    if (assetIndex > -1) {
      this.setState((prevState) => {
        let newStory = prevState.story
        newStory.assets[assetIndex].addNewEvent()
        return {story: newStory}
      })
    }
  }

  deleteEvent (assetIdx, eventIdx) {
    console.log('firing', assetIdx, eventIdx)
    if (assetIdx > -1 && eventIdx > -1) {
      this.setState((prevState) => {
        let newStory = prevState.story
        newStory.assets[assetIdx].events = newStory.assets[assetIdx].events.filter((event, i) => {return i !== eventIdx})
        console.log(newStory.assets[assetIdx].events)
        return {story: newStory}
      })
    }

  }

  setAttributesForScrollPoint (scrollPoint) {
    this.setState((prevState) => {
      return {assets: prevState.assets.map((asset)=>{
        asset.getCurrentAttributesForAllEvents(scrollPoint)
      })}

    })
  }

  render () {
    if (this.state.loading) return <Loading />

    return (
      <div className="CMS">
        <ImageUploader
          showImageUploader={this.state.showImageUploader}
          addAsset={this.addAsset}
          toggleImageUploader={this.toggleImageUploader}
           />


        <StoryInfoPopover
          active={this.state.showStoryInfoPopover}
          handleStoryInfoEdit={this.handleStoryInfoEdit}
          toggleStoryInfoPopover={this.toggleStoryInfoPopover}
          {...this.state.story}
           />
           <div className={this.state.fader ? 'active fader' : 'fader'} onClick={(e)=>{
            if (this.state.showStoryInfoPopover) this.toggleStoryInfoPopover(e)
            if (this.state.showImageUploader) this.toggleImageUploader()
          }}></div>
        <AssetContainer
          selected={this.state.selected.idx}
          setSelected={this.setSelected}
          assets={this.state.story.assets}
          onSortEnd={this.onSortEnd}
          toggleImageUploader={this.toggleImageUploader}
          deleteAsset={this.deleteAsset}
          hideAsset={this.hideAsset}
          toggleStoryInfoPopover={this.toggleStoryInfoPopover}
          title={this.state.story.title}
          saveStory={this.saveStory}
          editAsset={this.editAsset}
          dateModified={this.state.story.dateModified}
 />
        <PreviewContainer
          scrollPoint={this.state.scrollPoint}
          max={this.state.story.max}
          setScrollPoint={this.setScrollPoint}
          playPreview={this.playPreview}
          stopPreview={this.stopPreview}
          assets={this.state.previewAssets}
          playing={this.state.playing}
        />
        <EventContainer
          story={this.state.story}
          editEventProperty={this.editEventProperty}
          addEvent={this.addEvent}
          deleteEvent={this.deleteEvent}
          scrollPoint={this.state.scrollPoint}
          selected={this.state.selected}
          update={this.updateMain}
          flagAssetsAndEvents={this.flagAssetsAndEvents}
          presentInfoPopover={this.props.presentInfoPopover}
          validateEvent={this.validateEvent}
          saveStory={this.saveStory}
         />
      </div>
      )
  }
}

