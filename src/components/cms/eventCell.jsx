import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

export default class EventCell extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      expanded: false
    }
    this.expand = this.expand.bind(this)
  }

  expand () {
    this.setState((prevState) => {return {expanded: !prevState.expanded}})
  }

  makeJSXForm (type, time) {
    if (type === 'scrollPoint') {
      return (<div>
                <label >{type}:</label>
                <input type="number" value={this.props[time]} onChange={(e) => {
                  this.props.editEventProperty(e, time, this.props.idx)
                }} min={0} onBlur={(e)=>{this.props.validate(this.props.idx, type)}} />
              </div>)
    } else {

    const style = time +  'Style'

    if (type === 'alpha') {
      return (<div>
                <label >{'opacity'}:</label>
                <input type="number" value={this.props[style][type]} onChange={(e) => {
                  this.props.editEventProperty(e, type, this.props.idx, time)
                }} min={0} max={1} onBlur={(e)=>{this.props.validate(this.props.idx, type)}} />
              </div>)
    }
    let label = type
    if (type[0] === 'x') label = 'x'
    if (type[0] === 'y') label = 'y'
    return (<div>
              <label >{label}:</label>
              <input type="number" value={this.props[style][type]} onChange={(e) => {
                this.props.editEventProperty(e, type, this.props.idx, time)
              }} min={0} onBlur={(e)=>{this.props.validate(this.props.idx, type)}} />
            </div>)
    }

  }

  render () {

    let buttons = this.state.expanded ?
      ( <div className='eventButtons'>
          <button className='eventDelete' onClick={()=>{
            this.props.deleteEvent(this.props.assetIndex, this.props.idx)
          }}><FontAwesomeIcon icon={faTrashAlt} /></button>
          <button className="expandArrow" onClick={this.expand} >{this.state.expanded ? "▲" : "▼"}</button>

        </div>
      )
      :
      (<div></div>)

      let liClassName = 'event'
      if (this.props.scrollPoint >= this.props.start && this.props.scrollPoint <= this.props.end) liClassName += ' highlighted'

        if (this.state.expanded) {
          return (
            <li className={"expandedEvent"}>
              <div className="eventForms">
                <div className='left'>
                  <h4>start</h4>
                    <label >start:</label>
                    <input type="number" value={this.props.start} onChange={(e) => {
                      this.props.editEventProperty(e, 'start', this.props.idx)
                    }} min={0} />
                    <label className={!this.props.nonDefaultTypes.hasOwnProperty('xPos') ? "" : 'greyOut'}>position </label>
                  <div className="inputContainer">
                    {this.makeJSXForm('xPos', 'start')}
                    {this.makeJSXForm('yPos', 'start')}
                  </div>
                    {this.makeJSXForm('alpha', 'start')}

                    <label className={!this.props.nonDefaultTypes.hasOwnProperty('xScale') ? "" : 'greyOut'}>scale </label>
                  <div className="inputContainer">
                    {this.makeJSXForm('xScale', 'start')}
                    {this.makeJSXForm('yScale', 'start')}
                  </div>
                    {this.makeJSXForm('rotation', 'start')}
                </div>

                <div className='right'>
                  <h4>end</h4>
                  {this.makeJSXForm('scrollPoint', 'end')}
                    <label className={!this.props.nonDefaultTypes.hasOwnProperty('xPos') ? "" : 'greyOut'}>position </label>
                  <div className="inputContainer">
                    {this.makeJSXForm('xPos', 'end')}
                    {this.makeJSXForm('yPos', 'end')}
                  </div>
                    {this.makeJSXForm('alpha', 'end')}

                    <label className={!this.props.nonDefaultTypes.hasOwnProperty('xScale') ? "" : 'greyOut'}>scale </label>
                  <div className="inputContainer">
                    {this.makeJSXForm('xScale', 'end')}
                    {this.makeJSXForm('yScale', 'end')}
                  </div>
                    {this.makeJSXForm('rotation', 'end')}
                </div>
              </div>
                {buttons}


            </li>
          )
        }


    return (
      <li className={liClassName}>

        <div className='left'>
          <h4>start</h4>
          <p className={this.props.flagged.hasOwnProperty('scrollPoints') ? "flagged" : ''}>scrollpoint: {this.props.start}</p>
          <p>position: ({this.props.startStyle.xPos}, {this.props.startStyle.yPos})</p>
            <p>opacity: {this.props.startStyle.alpha}</p>
            <p>scale: ({this.props.startStyle.xScale}, {this.props.startStyle.yScale})</p>
            <p>rotation: {this.props.startStyle.rotation} deg</p>
        </div>

        <div className='right'>
        <h4>end</h4>
          <p className={this.props.flagged.hasOwnProperty('scrollPoints') ? "flagged" : ''}>scrollpoint: {this.props.end}</p>
          <p>position: ({this.props.endStyle.xPos}, {this.props.endStyle.yPos})</p>
          <p>opacity: {this.props.endStyle.alpha}</p>
          <p>scale: ({this.props.endStyle.xScale}, {this.props.endStyle.yScale})</p>
          <p>rotation: {this.props.endStyle.rotation} deg</p>
        </div>
        <button className="expandArrow" onClick={this.expand} >{this.state.expanded ? "▲" : "▼"}</button>


      </li>
    )
  }
}
