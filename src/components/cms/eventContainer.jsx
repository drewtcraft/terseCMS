import React from 'react';
import EventCell from './eventCell.jsx'
import {Event} from '../../other/classes.js';
import '../../styles/cms.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSave } from '@fortawesome/free-regular-svg-icons'
import {  faPlusCircle } from '@fortawesome/free-solid-svg-icons'


library.add(faSave)


export default class EventContainer extends React.Component {
  constructor(props){
    super(props)

  }

  componentDidMount () {
    this.setState({
      events: this.props.events
    })
  }

  deleteEvent (idx) {
    this.setState((prevState)=>{
      return {
        events: prevState.events.filter((_, _idx) => idx !== _idx)
      }
    })
  }

  render () {
    let eventsFor = this.props.selected.assetName ? `Events for: ${this.props.selected.assetName}` : 'No asset selected'

    const idx = this.props.selected.idx

    const asset = idx !== null ? this.props.story.assets[idx] : undefined
    const events = asset ? asset.events : []

    console.log('idx', idx)

    let eventCells = events.map((event, i)=>{
      return <EventCell
        {...event}
        key={i}
        idx={i}
        editEventProperty={this.props.editEventProperty}
        addEvent={this.props.addEvent}
        deleteEvent={this.props.deleteEvent}
        scrollPoint={this.props.scrollPoint}
        assetIndex={idx}
        validate={this.props.validateEvent}
         />


    })



    return(
    <div className="eventContainer">
      <h4>{eventsFor}</h4>
      <button className={this.props.selected.idx !== null ? "add" : "hidden"} onClick={(e)=>{
        this.props.addEvent()
      }}><FontAwesomeIcon icon={faPlusCircle} /></button>
      <ul className="eventList">
        {eventCells}
      </ul>

    </div>)
  }

}
