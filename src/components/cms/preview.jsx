import React from 'react';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css'

export default function PreviewContainer (props) {


    let assets = props.assets.map((asset, i) => {
      return (<img style={asset.currentStyle} key={i} src={asset.image} className={asset.hidden ? "previewAsset hidden" : "previewAsset"} />)
    })

    let button = (<button className="play" alt="pause story" title="play story" onClick={(()=>{
        props.playPreview(props.scrollPoint)
      })}>▶</button>)
    if (props.playing) {
      button = (<button className="pause" alt="pause story" title="pause story" onClick={()=>{
        props.stopPreview()
      }}>||</button>)
    }
    return (
      <div className="previewContainer">
        <div className="scrollControl">
          <div>
            {button}
          </div>
          <input type="number" value={props.scrollPoint} onChange={(e)=>{props.setScrollPoint(e.target.value)}} />
          <Slider
            min={0}
            max={props.max}
            value={props.scrollPoint}
            onChange={props.setScrollPoint}
            tooltip={false} />
        </div>
        <div className="screen">
          {assets}
        </div>
      </div>
    )

}
