import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import './../../styles/userAuth.css'

export default class Login extends React.Component {
  constructor (props) {
    super (props)

  }

  submit = (e) => {
    e.preventDefault()
    axios.post('http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/auth/login', {
      email: this.email.value,
      password: this.password.value
    })
    .then((tokenData)=>{
      console.log('aqui', tokenData.data)
      window.localStorage.setItem('terseCMSToken', tokenData.data.token)
      this.props.setUser(tokenData.data.username)
      this.props.redirect('/')
    })
    .catch((err) => {console.log(err)})
  }

  render () {
    return (
      <div className="userAuth login">
        <Link to={{pathname:'/register'}} className="switchAuth">register</Link>
        <form onSubmit={this.submit}>
          <h4>Log In</h4>
          <input type="email" ref={(ref) => {this.email = ref}} placeholder='email' />
          <input type="password" ref={(ref) => {this.password = ref}} placeholder='password' />
          <button type="submit">log in</button>
        </form>
      </div>
      )
  }
}
