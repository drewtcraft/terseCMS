import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle, faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import './../../styles/userAuth.css'

library.add(faCircle)
library.add(faCheckCircle)


export default class Register extends React.Component {
  constructor (props) {
    super (props)
    this.state = {
      subscribed: false
    }

  }

  validate = () => {
    if (this.password.value !== this.confirmPassword.value) {
      console.log(this.password.value, this.confirmPassword.value)
      this.props.presentInfoPopover('error', 'passwords do not match')
      return
    }
    if (this.username.value === '' || this.password.value === '' || this.email.value === '') {
      this.props.presentInfoPopover('error', 'some fields were left blank')
      return
    }
    return true


  }

  submit = (e) => {
    e.preventDefault()
    if (this.validate()) {
      axios.post('http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/auth/register', {
          username: this.username.value,
          password: this.password.value,
          email: this.email.value,
          subscribed: this.state.subscribed
        })
        .then((data)=>{
          console.log(data.data)
          window.localStorage.setItem('terseCMSToken', data.data)
          this.props.setUser(data.data.username)
          this.props.redirect('/')
        })
        .catch((err) => {console.log(err)})
    }
  }

  render () {
    return (
      <div className="userAuth register">
        <Link to={{pathname:'/login'}} className="switchAuth">log in</Link>
        <form onSubmit={this.submit}>
          <h4>Register</h4>
          <input type="text" ref={(ref) => {this.username = ref}} placeholder='desired username' />
          <input type="email" ref={(ref) => {this.email = ref}} placeholder='email' />
          <input type="password" ref={(ref) => {this.password = ref}} placeholder='password' />
          <input type="password" ref={(ref) => {this.confirmPassword = ref}} placeholder=' re-enter password' />
          <div>
          <FontAwesomeIcon className="checkbox" onClick={()=>{this.setState((prevState)=>{return{subscribed: !prevState.subscribed}})}} icon={this.state.subscribed ? "check-circle" : "circle"} />
          <p>Subscribe to Terse's mailing list. No more than one e-mail a week, if that. We will never sell your information</p>
          </div>
          <button type="submit">register</button>
        </form>
      </div>
      )
  }
}
