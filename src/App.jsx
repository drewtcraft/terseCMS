import React, { Component } from 'react';
import { withRouter } from 'react-router';
import {Switch, Route} from 'react-router-dom';
import Header from './components/headerFooter/header.jsx'
import MainEditor from './components/cms/mainEditor.jsx'
import Register from './components/userAuth/register.jsx'
import Login from './components/userAuth/login.jsx'
import MyStories from './components/myStories/myStories.jsx';
import InfoPopover from './components/InfoPopover.jsx';
import requireAuth from './components/higherOrderComponents/RequireAuth.jsx';
import Landing from './components/landing.jsx'
import TutorialFeed from './components/tutorials/tutorialFeed.jsx'
import axios from 'axios';
import './styles/App.css'

const util = require('util');


class App extends Component {
  constructor (props) {
    super (props)
    this.state = {
      user: null,
      showInfo: false,
      info: {title: '', text: ''}
    }

  }

  // on mounting, authenticate
  componentWillMount () {

    this.checkToken()
  }

  // on route change, authenticate
  componentDidUpdate (prevProps) {
    if (this.props.location !== prevProps.location) {
          //this.checkToken()
        }
  }

  // token verification
  checkToken = () => {
    // is there a token?
    const token = window.localStorage.getItem('terseCMSToken')
    console.log(token)
    if (token) {
      // if so, verify it, then set the state
      axios.post('http://ec2-18-218-138-61.us-east-2.compute.amazonaws.com:5000/auth/token', {token: token})
        .then( user => {
          console.log(user)
          this.setState({user: user.data})
        })
        .catch( err => {
          console.log(err)
          this.setState({user: null})})
    }
  }

  destroyToken = () => {
      window.localStorage.removeItem('terseCMSToken')
      this.setState({user: null})
  }

  presentInfoPopover = (title, text) => {
    this.setState({
      showInfo: true,
      info: {title: title, text: text}
    })
  }

  hidePopover = (e) => {
    e.preventDefault
    this.setState({
      showInfo: false
    })
  }

  redirect = (location) => {
    this.props.history.push(location)
  }

  setUser = (username) => {
    this.setState({user: username})
  }

  setID = (id) => {
    console.log(id)
    this.setState({id: id})
  }



  render () {

    return (
      <div className="App">

        <InfoPopover {...this.state.info} showInfo={this.state.showInfo} hidePopover={this.hidePopover} />
        <div className={this.state.showInfo ? "infoFader" : "hidden"} onClick={this.hidePopover}></div>

        <Header

          location={this.props.location.pathname}
          user={this.state.user}
          logOut={this.destroyToken}
        />

        <Switch>

          <Route exact path={`/`} render={()=>{
            return <Landing />
          }} />

          <Route exact path={`/tutorials`} render={()=>{
            return <TutorialFeed />
          }} />

          <Route exact path={`/story`} render={()=>{
            return (requireAuth(MainEditor, {presentInfoPopover: this.presentInfoPopover, user: this.state.user, id: this.state.id}))
            }} />

          <Route exact path={`/newStory`} render={()=>{
            return (requireAuth(MainEditor, {presentInfoPopover: this.presentInfoPopover, user: this.state.user, new: true}))
            }} />

          <Route exact path={`/stories`} render={()=>{
            return (requireAuth(MyStories, {presentInfoPopover: this.presentInfoPopover, user: this.state.user, setID: this.setID, new: false}))
          }} />

          <Route exact path={`/register`} render={()=>{return(
            <Register
              setUser={this.setUser}
              redirect={this.redirect}
              presentInfoPopover={this.presentInfoPopover}
            />
            )}} />

          <Route exact path={`/login`} render={()=>{return(
            <Login
              setUser={this.setUser}
              redirect={this.redirect}
              presentInfoPopover={this.presentInfoPopover}
            />
            )}} />

        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
